# SimpleSearchBar

A simple, working search bar for HTML websites.

## **How it works**
By clicking on the arrow the search bar is extended. As soon as the user enters text, `keyup` starts the `search()` function, which uses `filter` to hide all `<article>` elements that do not contain the search query. All other articles remain.


![Demo GIF](https://codeberg.org/pixelcode/SimpleSearchBar/raw/branch/master/SimpleSearchBear.gif)



**Clicking on the left arrow extends the search bar:**

```
$('#leftarrow').click(function(){
	$('#search').animate({ 'right': '0px' });
	$('#leftarrow').css({ 'display': 'none' });
	$('#rightarrow').css({ 'display': 'inline-block' });
});
```

**Clicking on the right arrow collapses the search bar:**

```
$('#rightarrow').click(function(){
	$('#search').animate({ 'right': '-225px' });
	$('#leftarrow').css({ 'display': 'inline-block' });
	$('#rightarrow').css({ 'display': 'none' });
});
```

**Typing into the search bar or clicking on the search button starts the search, so that the results are refreshed with each character. That's live search!**

```
$('#searchfield').keyup(function(){
	search();
});

$('#searchbutton').click(function(){
	search();
});
```
**That's the actual `search()` function:**

```
function search(){
	var searchquery = document.getElementById('searchfield').value.toLowerCase();
	$('article').css({ 'display': 'block' });
	$('article').filter(function () {
		return ($(this).find('*').text().toLowerCase().indexOf(searchquery) == -1)
	}).css({ 'display': 'none' });
}
```
Any `article` that does not have a child element containing the search query will be kicked out.

## **I want to search other elements**
No problem, just change all occurrences of `article` in the `script.js` file to your element type or CSS class.

For example, if your element type is `p` then change all `article` occurrences to `p`, e.g.:

| change                                      | to                                    |
| ------------------------------------------- | ------------------------------------- |
| `$('article').css({ 'display': 'block' });` | `$('p').css({ 'display': 'block' });` |



## **License**
You may freely use **SimpleSearchBar's** source code according the [MIT licence](https://codeberg.org/pixelcode/SimpleSearchBar/src/branch/master/LICENCE.md) (slightly modified). A link to my repo as the original source would be great 😎


-----


**jQuery**: SimpleSearchBar uses [jQuery](https://jquery.com).

**Font Awesome**: SimpleSearchBar uses [Font Awesome](https://fontawesome.com) icons. These may be used under the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0) license.